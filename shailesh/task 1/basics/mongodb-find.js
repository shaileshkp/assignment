const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/mousedb';

MongoClient.connect(url, (err, db) => {
	if (err) {
		return console.log('Unable to connect to database');
	} 
	console.log('Connected to database');

	db.collection('Positions').find({_id:"40:30"}).toArray().then((docs) => {
		console.log('Positions');
		console.log(JSON.stringify(docs, undefined, 2));
	}, (err) => {
		console.log('Unable to fetch positions',err);
	});
	db.close();
});