const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/mousedb';

MongoClient.connect(url, (err, db) => {
	if (err) {
		return console.log('Unable to connect to database');
	} 
	console.log('Connected to database');

	db.collection('Positions').findOneAndUpdate({
		_id:'440:300'
	}, {
		$set: {
			text: 'Hello world'
		}
	}, {
		returnOriginal: false
	}).then(result => {
		console.log(result);
	}); 

	db.close();
});