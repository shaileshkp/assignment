var mongoose = require('mongoose');

var Position = mongoose.model('Position', {
	_id: {
		type: String,
		required: true
	},
	cordX: {
		type: Number,
		required: true
	},
	cordY: {
		type: Number,
		required: true
	},
	text: {
		type: String,
		required: true,
		minlength:1,
		trim: true,
		default: 'Hello shailesh'
	}
});

module.exports = { Position };
